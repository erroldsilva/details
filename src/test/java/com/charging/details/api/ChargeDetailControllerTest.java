package com.charging.details.api;

import com.charging.details.dto.ChargeDetailSessionDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.Date;



@AutoConfigureMockMvc
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest
@ComponentScan(basePackages = "com.charging.details")
public class ChargeDetailControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testCreateChargeDetail_ValidInput() throws Exception {
        ChargeDetailSessionDto chargeDetailDto = ChargeDetailSessionDto.builder()
                .sessionIdentification("session123")
                .vehicleIdentification("vehicle456")
                .startTime(new Date())
                .endTime(new Date(System.currentTimeMillis() + 10))
                .totalCost(BigDecimal.valueOf(50.0))
                .build();

        ObjectMapper objectMapper = new ObjectMapper();
        String chargeDetailDtoJson = objectMapper.writeValueAsString(chargeDetailDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/charge-details/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(chargeDetailDtoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }


    @Test
    public void testCreateChargeDetail_EndTimeBeforeStartTime() throws Exception {
        ChargeDetailSessionDto chargeDetailDto = ChargeDetailSessionDto.builder()
                .sessionIdentification("session123")
                .vehicleIdentification("vehicle456")
                .startTime(new Date())
                .endTime(new Date(System.currentTimeMillis() - 1000))
                .totalCost(BigDecimal.valueOf(50.0))
                .build();


        mockMvc.perform(MockMvcRequestBuilders.post("/charge-details/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToJson(chargeDetailDto)))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());
    }

    @Test
    public void testCreateChargeDetail_InvalidCost() throws Exception {
        ChargeDetailSessionDto chargeDetailDto = ChargeDetailSessionDto.builder()
                .sessionIdentification("session123")
                .vehicleIdentification("vehicle456")
                .startTime(new Date())
                .endTime(new Date())
                .totalCost(BigDecimal.valueOf(0))
                .build();


        mockMvc.perform(MockMvcRequestBuilders.post("/charge-details/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToJson(chargeDetailDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testCreateChargeDetail_StartTimeIsAheadOfPreviousEndTime() throws Exception {
        ChargeDetailSessionDto chargeDetailDtoFirst = ChargeDetailSessionDto.builder()
                .sessionIdentification("session123")
                .vehicleIdentification("vehicle456")
                .startTime(new Date())
                .endTime(new Date())
                .totalCost(BigDecimal.valueOf(100))
                .build();


        ChargeDetailSessionDto chargeDetailDtoSecond = ChargeDetailSessionDto.builder()
                .sessionIdentification("session123")
                .vehicleIdentification("vehicle456")
                .startTime(new Date(System.currentTimeMillis() - 10000) )
                .endTime(new Date())
                .totalCost(BigDecimal.valueOf(100))
                .build();
        mockMvc.perform(MockMvcRequestBuilders.post("/charge-details/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToJson(chargeDetailDtoFirst)))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.post("/charge-details/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToJson(chargeDetailDtoSecond)))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());
    }

    private String convertToJson(Object object) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
