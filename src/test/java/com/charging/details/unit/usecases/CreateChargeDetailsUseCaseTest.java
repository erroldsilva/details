package com.charging.details.unit.usecases;

import com.charging.details.dto.ChargeDetailSessionDto;
import com.charging.details.models.ChargeDetailsSession;
import com.charging.details.service.usecases.CreateChargeDetailsUseCase;
import com.charging.details.service.usecases.exception.InvalidTimeException;
import com.charging.details.service.usecases.exception.InvalidTotalCostException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class CreateChargeDetailsUseCaseTest {


    @Test
    public void create_ValidChargeDetails_ReturnsChargeDetailsSession() throws Exception {
        ChargeDetailSessionDto details = ChargeDetailSessionDto.builder()
                .startTime(new Date())
                .endTime(new Date())
                .totalCost(BigDecimal.valueOf(1))
                .build();

        ChargeDetailsSession lastChargeDetail = new ChargeDetailsSession();
        setEndTimeValue(lastChargeDetail, new Date(System.currentTimeMillis() - 1000));
        ChargeDetailsSession result = CreateChargeDetailsUseCase.create(details, lastChargeDetail);

        assertNotNull(result);
    }

    @Test
    public void create_EndTimeOfLastSessionIsAfterStartTime_ValidChargeDetails_ReturnsChargeDetailsSession() throws Exception {
        ChargeDetailSessionDto details = ChargeDetailSessionDto.builder()
                .startTime(new Date())
                .endTime(new Date())
                .totalCost(BigDecimal.valueOf(1))
                .build();

        ChargeDetailsSession lastChargeDetail = new ChargeDetailsSession();
        setEndTimeValue(lastChargeDetail, new Date());
        ChargeDetailsSession result = CreateChargeDetailsUseCase.create(details, lastChargeDetail);

        assertNotNull(result);
    }

    @Test
    public void create_EndTimeOfLastSessionIsBeforeStartTime_ThrowsInvalidTimeException() throws Exception {
        ChargeDetailSessionDto details = ChargeDetailSessionDto.builder()
                .startTime(new Date())
                .endTime(new Date())
                .totalCost(BigDecimal.valueOf(1))
                .build();

        ChargeDetailsSession lastChargeDetail = new ChargeDetailsSession();
        setEndTimeValue(lastChargeDetail, new Date((System.currentTimeMillis() + 1000)));
        assertThrows(InvalidTimeException.class, () -> CreateChargeDetailsUseCase.create(details, lastChargeDetail));
    }

    @Test
    public void create_EndTimeBeforeStartTime_ThrowsInvalidTimeException() {
        ChargeDetailSessionDto details = ChargeDetailSessionDto.builder()
                .startTime(new Date())
                .endTime(new Date(System.currentTimeMillis() - 10))
                .totalCost(BigDecimal.valueOf(1))
                .build();

        assertThrows(InvalidTimeException.class, () -> CreateChargeDetailsUseCase.create(details, null));
    }

    @Test
    public void create_TotalCostIsZero_ThrowsInvalidTotalCostException() {
        ChargeDetailSessionDto details = ChargeDetailSessionDto.builder()
                .startTime(new Date())
                .endTime(new Date())
                .totalCost(BigDecimal.ZERO)
                .build();


        assertThrows(InvalidTotalCostException.class, () -> CreateChargeDetailsUseCase.create(details, null));
    }

    @Test
    public void create_TotalCostIsNegative_ThrowsInvalidTotalCostException() {
        ChargeDetailSessionDto details = ChargeDetailSessionDto.builder()
                .startTime(new Date())
                .endTime(new Date())
                .totalCost(BigDecimal.valueOf(-1))
                .build();

        assertThrows(InvalidTotalCostException.class, () -> CreateChargeDetailsUseCase.create(details, null));
    }

    private void setEndTimeValue(ChargeDetailsSession chargeDetailsSession, Date value) throws Exception {
        Field field = chargeDetailsSession.getClass().getDeclaredField("endTime");
        field.setAccessible(true);
        field.set(chargeDetailsSession, value);
    }
}
