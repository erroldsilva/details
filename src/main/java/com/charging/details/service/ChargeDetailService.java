package com.charging.details.service;

import com.charging.details.dto.ChargeDetailSessionDto;
import com.charging.details.dto.ChargeDetailsResponseDto;
import com.charging.details.dto.ChargeDetailsSearchCriteriaDto;
import com.charging.details.mappers.ChargeDetailSessionMapper;
import com.charging.details.mappers.ChargeDetailsSearchResultMapper;
import com.charging.details.models.ChargeDetailsSession;
import com.charging.details.repositories.ChargeDetailRepository;
import com.charging.details.service.usecases.CreateChargeDetailsUseCase;
import com.charging.details.service.usecases.exception.ChargeDetailNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChargeDetailService {
    private final ChargeDetailRepository chargeDetailRepository;

    @Autowired
    public ChargeDetailService(ChargeDetailRepository chargeDetailRepository) {
        this.chargeDetailRepository = chargeDetailRepository;
    }

    public String createChargeDetail(ChargeDetailSessionDto chargeDetail) {
        Optional<ChargeDetailsSession> lastChargeDetailSession = chargeDetailRepository
                .findFirstByVehicleIdentificationAndSessionIdentificationOrderByEndTimeDesc(
                        chargeDetail.getVehicleIdentification(),
                        chargeDetail.getSessionIdentification()
                );

        ChargeDetailsSession newChargeDetail = CreateChargeDetailsUseCase
                .create(
                        chargeDetail,
                        lastChargeDetailSession.orElse(null)
                );

        return chargeDetailRepository.save(newChargeDetail).getChargeDetailId();
    }

    public ChargeDetailsResponseDto getPaginatedSessionsByVehicleWithSort(ChargeDetailsSearchCriteriaDto params) {
        PageRequest pageRequest = PageRequest.of(
                params.getPage(),
                params.getPageSize(),
                params.getSortOrder(),
                params.getSortField()
        );
        Page<ChargeDetailsSession> result = chargeDetailRepository.findByVehicleIdentification(params.getVehicleIdentification(), pageRequest);
        return ChargeDetailsSearchResultMapper.toDTO(result);
    }

    public ChargeDetailSessionDto getChargeDetailById(String chargeDetailId) {
        Optional<ChargeDetailsSession> chargeDetailOptional =  chargeDetailRepository.findByChargeDetailId(chargeDetailId);
        ChargeDetailsSession result = chargeDetailOptional.orElseThrow(() -> new ChargeDetailNotFoundException("Charge detail session not found with ID: " + chargeDetailId));
        return ChargeDetailSessionMapper.toDTO(result);
    }
}
