package com.charging.details.service.usecases.exception;

public class ChargeDetailNotFoundException extends RuntimeException {
    public ChargeDetailNotFoundException(String message) {
        super(message);
    }

}