package com.charging.details.service.usecases;

import com.charging.details.dto.ChargeDetailSessionDto;
import com.charging.details.mappers.ChargeDetailSessionMapper;
import com.charging.details.models.ChargeDetailsSession;
import com.charging.details.service.usecases.exception.InvalidTimeException;
import com.charging.details.service.usecases.exception.InvalidTotalCostException;

import java.math.BigDecimal;
import java.util.Date;

public class CreateChargeDetailsUseCase {

    public static ChargeDetailsSession create(
            ChargeDetailSessionDto details,
            ChargeDetailsSession lastChargeDetail
    ) {
        ensureEndTimeOfLastSessionIsBeforeStartTimeOfTheCurrentSession(details, lastChargeDetail);
        ensureEndTimeIsAfterStartTime(details);
        ensureTotalCostIsMoreThanZero(details);

        return ChargeDetailSessionMapper.toEntity(details);
    }

    static void ensureEndTimeOfLastSessionIsBeforeStartTimeOfTheCurrentSession(
            ChargeDetailSessionDto details,
            ChargeDetailsSession lastChargeDetail
    ) {
        if (lastChargeDetail != null) {
            Date lastEndTime = lastChargeDetail.getEndTime();
            Date newStartTime = details.getStartTime();

            if (lastEndTime.after(newStartTime)) {
                throw new InvalidTimeException("End time of the last charge detail is after the start time of the new charge detail.");
            }
        }
    }

    static void ensureEndTimeIsAfterStartTime(ChargeDetailSessionDto detail) {
        Date endTime = detail.getEndTime();
        Date startTime = detail.getStartTime();
        if (endTime.before(startTime)) {
            throw new InvalidTimeException("End time of the charge details cannot be before start time.");
        }
    }

    static void ensureTotalCostIsMoreThanZero(ChargeDetailSessionDto detail) {
        BigDecimal totalCost = detail.getTotalCost();
        if (totalCost.compareTo(BigDecimal.ZERO) <= 0) {
            throw new InvalidTotalCostException("Total cost must be greater than zero");
        }
    }
}
