package com.charging.details.service.usecases.exception;

public class InvalidTotalCostException extends RuntimeException {
    public InvalidTotalCostException(String message) {
        super(message);
    }
}