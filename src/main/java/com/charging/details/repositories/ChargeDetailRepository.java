package com.charging.details.repositories;

import com.charging.details.models.ChargeDetailsSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
@NoRepositoryBean
interface CustomChargeDetailRepository extends JpaRepository<ChargeDetailsSession, Long> {

    Optional<ChargeDetailsSession> findFirstByVehicleIdentificationAndSessionIdentificationOrderByEndTimeDesc(
            String vehicleIdentification,
            String sessionIdentification
    );
    Page<ChargeDetailsSession> findByVehicleIdentification(String vehicleIdentification, Pageable pageable);

    Optional<ChargeDetailsSession> findByChargeDetailId(String chargeDetailId);
}

@Repository
public interface ChargeDetailRepository extends CustomChargeDetailRepository {
}