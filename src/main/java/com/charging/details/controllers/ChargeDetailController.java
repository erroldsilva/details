package com.charging.details.controllers;

import com.charging.details.dto.ChargeDetailSessionDto;
import com.charging.details.dto.ChargeDetailsResponseDto;
import com.charging.details.dto.ChargeDetailsSearchCriteriaDto;
import com.charging.details.service.ChargeDetailService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/charge-details")
@Tag(name = "Charge Details API", description = "Endpoints for managing charge details")
public class ChargeDetailController {

    private final ChargeDetailService chargeDetailService;

    @Autowired
    public ChargeDetailController(ChargeDetailService chargeDetailService) {
        this.chargeDetailService = chargeDetailService;
    }

    @PostMapping("/create")
    public ResponseEntity<String> createChargeDetail(
            @RequestBody @Valid ChargeDetailSessionDto chargeDetailDto) {
        String createdChargeDetailId = chargeDetailService.createChargeDetail(chargeDetailDto);
        return new ResponseEntity<>(createdChargeDetailId, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ChargeDetailSessionDto> getChargeDetailById(
            @Parameter(description = "Charge detail session ID", required = true)
            @PathVariable String id) {
        ChargeDetailSessionDto chargeDetail = chargeDetailService.getChargeDetailById(id);
        return new ResponseEntity<>(chargeDetail, HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<ChargeDetailsResponseDto> searchAndSortChargeDetails(
            @Parameter(description = "Vehicle identification", required = true)
            @RequestParam String vehicleIdentification,
            @Parameter(description = "Sort field (startTime, endTime)")
            @RequestParam(defaultValue = "startTime") String sortField,
            @Parameter(description = "Sort order (asc, desc)")
            @RequestParam(defaultValue = "asc") String sortOrder,
            @Parameter(description = "Page number")
            @RequestParam(defaultValue = "1") int page,
            @Parameter(description = "Page size")
            @RequestParam(defaultValue = "10") int pageSize
    ) {
        ChargeDetailsSearchCriteriaDto searchCriteria = ChargeDetailsSearchCriteriaDto.builder()
                .vehicleIdentification(vehicleIdentification)
                .sortField(sortField)
                .sortOrder(Sort.Direction.valueOf(sortOrder.toUpperCase()))
                .page(page - 1)
                .pageSize(pageSize)
                .build();
        ChargeDetailsResponseDto result =
                chargeDetailService.getPaginatedSessionsByVehicleWithSort(searchCriteria);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
