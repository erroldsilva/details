package com.charging.details.mappers;

import com.charging.details.dto.ChargeDetailsResponseDto;
import com.charging.details.models.ChargeDetailsSession;
import org.springframework.data.domain.Page;

public class ChargeDetailsSearchResultMapper {

    public static ChargeDetailsResponseDto toDTO(Page<ChargeDetailsSession> session) {
       return ChargeDetailsResponseDto.builder()
               .result(session.getContent())
               .currentPage(session.getNumber() + 1)
               .totalElements(session.getTotalElements())
               .totalPages(session.getTotalPages())
               .build();
    }
}
