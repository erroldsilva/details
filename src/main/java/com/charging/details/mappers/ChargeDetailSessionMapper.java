package com.charging.details.mappers;

import com.charging.details.dto.ChargeDetailSessionDto;
import com.charging.details.models.ChargeDetailsSession;

import java.util.UUID;

public class ChargeDetailSessionMapper {
    public static ChargeDetailSessionDto toDTO(ChargeDetailsSession chargeDetailsSession) {
        return ChargeDetailSessionDto.builder()
                .sessionIdentification(chargeDetailsSession.getSessionIdentification())
                .vehicleIdentification(chargeDetailsSession.getVehicleIdentification())
                .startTime(chargeDetailsSession.getStartTime())
                .endTime(chargeDetailsSession.getEndTime())
                .totalCost(chargeDetailsSession.getTotalCost())
                .build();
    }

    public static ChargeDetailsSession toEntity(ChargeDetailSessionDto chargeDetailSessionDTO) {
       return ChargeDetailsSession.builder()
               .sessionIdentification(chargeDetailSessionDTO.getSessionIdentification())
               .vehicleIdentification(chargeDetailSessionDTO.getVehicleIdentification())
               .startTime(chargeDetailSessionDTO.getStartTime())
               .endTime(chargeDetailSessionDTO.getEndTime())
               .totalCost(chargeDetailSessionDTO.getTotalCost())
               .chargeDetailId(UUID.randomUUID().toString())
               .build();
    }
}
