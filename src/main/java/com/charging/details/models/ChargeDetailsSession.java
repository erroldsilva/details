package com.charging.details.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "sessions")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ChargeDetailsSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "session_identification", nullable = false)
    private String sessionIdentification;

    @Column(name = "vehicle_identification", nullable = false)
    private String vehicleIdentification;

    @Column(name = "start_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(name = "end_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @Column(name = "total_cost", nullable = false, precision = 10, scale = 2)
    private BigDecimal totalCost;

    @Column(name = "charge_detail_id", nullable = false, length = 36)
    private String chargeDetailId;

}
