package com.charging.details.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
public class ChargeDetailSessionDto {
    @NotBlank(message = "Session identification is required")
    private String sessionIdentification;

    @NotBlank(message = "Vehicle identification is required")
    private String vehicleIdentification;

    @NotNull(message = "Start time is required")
    private Date startTime;

    @NotNull(message = "End time is required")
    private Date endTime;

    @NotNull(message = "Total cost is required")
    @Positive(message = "Total cost must be a positive value")
    private BigDecimal totalCost;

}