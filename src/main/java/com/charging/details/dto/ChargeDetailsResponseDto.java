package com.charging.details.dto;

import com.charging.details.models.ChargeDetailsSession;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ChargeDetailsResponseDto {
    private List<ChargeDetailsSession> result;
    private int currentPage;
    private int totalPages;
    private long totalElements;
}
