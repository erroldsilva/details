package com.charging.details.dto;


import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Sort;

@Data
@Builder
public class ChargeDetailsSearchCriteriaDto {
    private final String vehicleIdentification;
    private final  String sortField;
    private final Sort.Direction sortOrder;
    private final int page;
    private final int pageSize;
}
