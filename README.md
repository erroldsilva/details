# Charge API


The project is containerised with docker, 
* To run the project refer to [steps](#how-to-run-the-application)
* To test with the unit\api test refer to [steps](#how-to-run-the-tests)
* The endpoints are documented with Open API & Swagger can be accessed via [link](http://localhost:8080/swagger-ui/index.html)

## How to run the application

```bash
git clone ....
cd ...
./gradlew bootJar && docker compose up -d --build
```

## How to run the tests
```bash
docker compose up db -d
./gradlew test
```

## How to access the documentation
```bash
http://localhost:8080/swagger-ui/index.html
```

## Testing
* The business logic is covered with unit tests
* The API is tested with end-to-end testing
* The test cover some very basic scenarios and cases

## Project Design
* The application is built with layer architecture
* Data flow between the modules is done via DTOs
* The business logic is isolated in classes `com.charging.details.service.usecases` so it can be tested in isolation, this is a concept borrowed from clean architecture without the other complexities
* The implementation covers a very high level approach to building an API

## Known Issues
* The validation done is only for the business cases
* The DB is created via an init.sql and not migration for simplicity and as there is no real need of migration with the current scope of this API
* Caching, authentication, gateway is assumed to be handled outside the application scope
