CREATE DATABASE IF NOT EXISTS charge_details_service;

CREATE TABLE IF NOT EXISTS sessions (
                                        id INT AUTO_INCREMENT PRIMARY KEY,
                                        session_identification VARCHAR(255) NOT NULL,
                                        vehicle_identification VARCHAR(255) NOT NULL,
                                        start_time TIMESTAMP NOT NULL,
                                        end_time TIMESTAMP NOT NULL,
                                        total_cost DECIMAL(10, 2) NOT NULL,
                                        charge_detail_id CHAR(36) NOT NULL
);

CREATE INDEX charge_detail_id_index
    ON sessions (charge_detail_id);

CREATE INDEX sessions_start_time_index
    ON sessions (start_time);

CREATE INDEX sessions_end_time_index
    ON sessions (end_time);
--
-- DELIMITER //
-- CREATE TRIGGER validate_start_time_and_total_cost
--     BEFORE INSERT ON sessions
--     FOR EACH ROW
-- BEGIN
--     IF NEW.start_time >= NEW.end_time THEN
--         SIGNAL SQLSTATE '45000'
--         SET MESSAGE_TEXT = 'Start time must be before end time';
-- END IF;
--
-- IF NEW.total_cost <= 0 THEN
--         SIGNAL SQLSTATE '45000'
--         SET MESSAGE_TEXT = 'Total cost must be greater than 0';
-- END IF;
-- END;
-- //
-- DELIMITER ;
